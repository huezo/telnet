# Telnet

Telnet

## Imagen

```registry.gitlab.com/huezo/telnet:latest```

# Uso

```docker run --name telnet -it registry.gitlab.com/huezo/telnet:latest  IP PUERTO```


```docker run --name telnet -it registry.gitlab.com/huezo/telnet:latest  DNS PUERTO```

![Telnet1](https://i.imgur.com/diBodG7.png)

# Uso 2

```docker run --name telnet -it registry.gitlab.com/huezo/telnet:latest``` 



![Telnet2](https://i.imgur.com/orgMEfF.png)
